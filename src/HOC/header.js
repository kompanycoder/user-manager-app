import React, { Component } from 'react'

export default class Header extends Component {
    render() {
        return (
            <div>
                <h1 className="text-center">User management App</h1>
            </div>
        )
    }
}
